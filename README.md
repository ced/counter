counter
===

This package exists mainly because the use of the [atomic/sync](https://golang.org/pkg/sync/atomic/#pkg-overview) package seems to be discouraged.