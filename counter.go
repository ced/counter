package counter

import (
	"sync"
)

type msgKind int

const (
	getValue msgKind = iota
	setValue
	incValue
	decValue
)

type msg struct {
	kind msgKind
	value int
	answer chan int
}

type Counter struct {
	in chan msg
	quit chan struct{}
	wg sync.WaitGroup
}

// Dec decrements the counter by 1
func (c *Counter) Dec() {
	c.DecBy(1)
}

// DecBy decrements the counter by the given amount
func (c *Counter) DecBy(amt int) {
	c.in <- msg{kind: decValue, value: amt}
}

// Inc increments the counter by 1
func (c *Counter) Inc() {
	c.IncBy(1)
}

// IncBy increments the counter by the given amount
func (c *Counter) IncBy(amt int) {
	c.in <- msg{kind: incValue, value: amt}
}

// Value returns the current value of the counter
func (c *Counter) Value() int {
	resp := make(chan int)
	c.in <- msg{kind: getValue, answer: resp}
	return <-resp
}

// Reset sets the counter's value to zero
func (c *Counter) Reset() {
	c.Set(0)
}

// Set sets the counter's value
func (c *Counter) Set(to int) {
	c.in <- msg{kind: setValue, value: to}
}

// Start starts a goroutine to handle messages to the counter.
func (c *Counter) Start() {
	c.wg.Add(1)
	go func() {
		defer c.wg.Done()
		var val int

		for {
			select {
			case m := <-c.in:
				switch m.kind {
				case getValue:
					m.answer <- val
					close(m.answer)
				case setValue:
					val = m.value
				case incValue:
					val += m.value
				case decValue:
					val -= m.value
				}
			case <-c.quit:
				return
			}
		}
	}()
}

func (c *Counter) Stop() {
	close(c.quit)
	c.wg.Wait()
}

func NewCounter() *Counter {
	c := Counter{
		in: make(chan msg),
		quit: make(chan struct{}),
	}

	c.Start()

	return &c
}
