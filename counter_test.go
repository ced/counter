package counter

import (
	"testing"
)

func TestNewCounter(t *testing.T) {
	h := NewCounter()
	defer h.Stop()
	v := h.Value()
	if v != 0 {
		t.Errorf("wrong initial counter value; got %d, want %d", v, 0)
	}
}

func TestCounter_Start(t *testing.T) {
	h := NewCounter()
	defer h.Stop()
	// NewCounter also calls Start. Redundant calls should be ok; Just means more goroutines can respond to messages.
	h.Start()
	h.Inc()
	v := h.Value()
	if v != 1 {
		t.Errorf("wrong counter value; got %d, want %d", v, 1)
	}
}

func TestCounter_Stop(t *testing.T) {
	h := NewCounter()
	h.Stop()
}

func TestCounter_Inc(t *testing.T) {
	h := NewCounter()
	defer h.Stop()
	h.Inc()
	h.Inc()
	v := h.Value()
	if v != 2 {
		t.Errorf("wrong counter value; got %d, want %d", v, 2)
	}

	h.Dec()
	h.Inc()
	h.Inc()
	v = h.Value()
	if v != 3 {
		t.Errorf("wrong counter value; got %d, want %d", v, 3)
	}
}

func TestCounter_IncBy(t *testing.T) {
	h := NewCounter()
	defer h.Stop()
	amt := 4
	h.IncBy(amt)
	v := h.Value()
	if v != amt {
		t.Errorf("wrong counter value; got %d, want %d", v, amt)
	}
}

func TestCounter_Dec(t *testing.T) {
	h := NewCounter()
	defer h.Stop()
	h.Dec()
	v := h.Value()
	if v != -1 {
		t.Errorf("wrong counter value; got %d, want %d", v, -1)
	}

	h.IncBy(3)
	h.Dec()
	v = h.Value()
	if v != 1 {
		t.Errorf("wrong counter value; got %d, want %d", v, 1)
	}
}

func TestCounter_DecBy(t *testing.T) {
	h := NewCounter()
	defer h.Stop()
	amt := 4
	want := -amt
	h.DecBy(amt)
	v := h.Value()
	if v != want {
		t.Errorf("wrong counter value; got %d, want %d", v, want)
	}
}